# Dracula theme for Markdeep

This is a theme for [Markdeep](https://casual-effects.com/markdeep/), heavily based on the provided Slate theme's CSS.

## What's been changed
* Colors
* Header numbers were removed
* Template rewritten

## What's stayed the same
* Pretty much everything else